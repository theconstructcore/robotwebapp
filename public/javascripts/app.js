new Vue({
    el: '#app',
    data: {
        loading: true,
        title: 'ROSDS Real Robot CLIENT',
        status: false,
        status_old: false,
        robot_url: '',
        device_name: 'my_device',
        pingInterval: null,
        lastPing: null
    },
    methods: {
        turnOn() {
            this.status_old = false
            let data = { request: 'turnon', args: { deviceName: this.device_name } }
            axios.post('/', data).then((res) => {
                if (res.data.result) {
                    this.status = true
                    this.robot_url = res.data.data.url
                } else {
                    alert(res.data.message + ': ' + res.data.details)
                }
            }).catch((error) => {
                alert('Something went wrong, please try again')
            })
        },
        turnOff() {
            this.status_old = false
            let data = { request: 'turnoff', args: { deviceName: this.device_name } }
            axios.post('/', data).then((res) => {
                if (res.data.result) {
                    this.status = false
                    this.robot_url = ''
                } else {
                    alert(res.data.message + ': ' + res.data.details)
                }
            }).catch((error) => {
                alert('Something went wrong, please try again')
            })
        },
        doPing() {
            if (this.status || this.status_old) {
                axios.get('/ping').then((res) => {
                    if(res.data.data.connected) {
                        this.status_old = false
                        this.lastPing = res.data.data.latency
                    }
                }).catch((err) => {

                })
            }
        }
    },
    computed: {
        status_text: function () {
            return this.status ? 'Ready!' : 'Not ready'
        },
        robot_url_text: function () {
            return this.robot_url == '' ? '-- none --' : this.robot_url
        },
        latencyStyle: function() {
            if (this.lastPing < 150) {
                return 'color:green'
            }
            if (this.lastPing < 500) {
                return 'color:orange'
            }
            return 'color:red;'
        }
    },
    beforeMount() {
        axios.get('/status').then((res) => {
            let data = res.data.data
            this.device_name = data.hostname
            if (data.ready) {
                this.status_old = true
                this.status = true
                this.robot_url = '-- empty --'
            }
            this.loading = false
        }).catch((err) => {

        })
    },
    created() {
        this.pingInterval = window.setInterval(this.doPing, 1000)
    }
})
