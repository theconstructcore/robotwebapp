// express
var express = require('express')
var router = express.Router()

// system
var os = require('os')
var fs = require('fs')

// child processes
const spawn = require('child_process').spawn
const spawnSync = require('child_process').spawnSync

// some paths =)
const ETCHOSTS = '/etc/hosts'
const SOURCE = '/usr/share/rosds_connector/source.sh'
// some strings =)
const ADDEDBYHUSARNET = '# managed by Husarnet'
const ADDEDBYTC = '# Added by Theconstruct'
const PING_UNKNOWN = 'unknown host'

///////////////////
// define routes //
///////////////////
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' })
})

router.get('/status', function (req, res, next) {
  fs.readFile(ETCHOSTS, function (err, data) {
    let ready = false
    if (err) throw err;
    if (data.indexOf(ADDEDBYHUSARNET) >= 0) {
      ready = true
    }
    res.send({
      result: true,
      message: '',
      details: '',
      data: { ready: ready, hostname: os.hostname() }
    })
  });
})

router.get('/ping', function (req, res, next) {
  ping_response_out = spawnSync('/usr/share/rosds_connector/scripts/ping_rosds.sh').stdout.toString()
  ping_response_err = spawnSync('/usr/share/rosds_connector/scripts/ping_rosds.sh').stderr.toString()
  console.log(ping_response_out)
  console.log(ping_response_err)
  if (ping_response_out.includes(PING_UNKNOWN) || ping_response_err.includes(PING_UNKNOWN)) {
    res.send({
      result: true,
      message: '',
      details: '',
      data: { connected: false, latency: -1 }
    })
  } else {
    res.send({
      result: true,
      message: ping_response_out,
      details: ping_response_err,
      data: { connected: true, latency: ping_response_out.split('\n')[1].split(' ')[6].split('=')[1] }
    })
  }
})

router.post('/', function (req, res) {
  let obj = req.body
  let script = []

  // select and execute script
  switch (obj.request) {
    // Turn ON
    case 'turnon':
      res.send(turnOn(obj.args.deviceName))
      return // break

    // Turn OFF
    case 'turnoff':
      res.send(turnOff(obj.args.deviceName))
      return // break

    // None
    default:
      res.send({ result: false, message: 'No script found!' })
      return
  }

  // return
  res.send({ result: true, message: 'Done!' })
})

////////////////////
// helper methods //
////////////////////
const turnOn = function (deviceName) {
  // change source.sh
  // NAME_OF_YOUR_DEVICE=$HOSTNAME
  fs.writeFileSync(SOURCE, 'export ROS_IPV6=on')
  fs.appendFileSync(SOURCE, '\nexport ROS_HOSTNAME=' + deviceName)
  fs.appendFileSync(SOURCE, '\nexport ROS_MASTER_URI=http://master:11311')

  // change /etc/hosts
  script = ['/usr/share/rosds_connector/scripts/set_etc_hosts_mode.py', 'RemoteMode', deviceName]
  ret = executeSyncScript(script[0], script.splice(1))
  console.log(ret)
  if (![0, null].includes(ret.output) || (ret.error != undefined)) {
    return {
      result: false,
      message: 'Something went wrong, please try again or contact us',
      details: ret.err
    }
  }

  // get URL
  script = '/usr/share/rosds_connector/scripts/husarnet_url.sh'
  ret = executeSyncScript(script)
  console.log(ret)
  if (![0, null].includes(ret.output) || (ret.error != undefined)) {
    return {
      result: false,
      message: 'Something went wrong, please try again or contact us',
      details: ret.err
    }
  }

  // We check that the URL data has the correct format
  url_array = ret.err.split(' ')
  if (url_array[0] == "Go")
  {
    // This is only when the hostname given already is insid ethe etc hosts
    url_address = url_array[2]
  }else if(url_array[0] == "sudo:"){
    // This is due to removing the etc host names first and not finding them
    url_address = url_array[7]
  }else{
    url_address = "Sorry but errors happen...Please contact info@theconstructsim.coml"
  }

  // Done
  return {
    result: true,
    message: 'Success',
    details: '',
    data: { url: url_address }
  }

}

const turnOff = function (deviceName) {
  // change /etc/hosts
  script = ['/usr/share/rosds_connector/scripts/set_etc_hosts_mode.py', 'AutoMode', deviceName]
  ret = executeSyncScript(script[0], script.splice(1))
  console.log(ret)
  if (![0, null].includes(ret.output) || (ret.error != undefined)) {
    return {
      result: false,
      message: 'Something went wrong, please try again or contact us',
      details: ret.err + ret.error
    }
  }

  // Done
  return {
    result: true,
    message: 'Success',
    details: ''
  }
}

const executeSyncScript = function (script, args) {
  let ret = spawnSync(script, args)
  obj = {
    output: ret.status,
    out: ret.stdout == null ? '' : ret.stdout.toString(),
    err: ret.stderr == null ? '' : ret.stderr.toString()
  }
  return obj
}

/////////
// end //
/////////
module.exports = router
