#!/bin/bash
export USER=$1
export NVM_DIR="/home/${USER}/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm use v10.15.3
node /usr/share/rosds_connector/bin/www
